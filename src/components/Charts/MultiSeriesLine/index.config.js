export default {
  header: 45,
  multiple: 12,
  padding: 30,
  height: 500,
  width: 750,
  x: 0,
  y: 0,
  chart: {
    type: 'msline',
    theme: 'fusion',
    baseFont: 'Roboto',
    borderAlpha: 100,
    borderColor: '#8c8278',
    borderThickness: 1,
    caption: "Hourly Revenue, Clicks and Impressions (000's)",
    drawCrossline: '1',
    captionFontSize: 20,
    captionPadding: 20,
    chartBottomMargin: 10,
    chartRightMargin: 30,
    chartTopMargin: 25,
    dataFormat: 'json',
    decimals: '2',
    divLineAlpha: 100,
    formatNumber: '1',
    labelStep: 24,
    lineThickness: '3',
    logoPosition: 'tl',
    logoUrl: 'assets/logo-small.png',
    logoLeftMargin: 11,
    logoTopMargin: 10,
    plotToolText: '<b>$seriesName: $dataValue</b>',
    showAnchors: '0',
    showBorder: '1',
    subCaption: '',
    subCaptionFontSize: 16,
    yAxisName: "$ Revenue / Clicks / Impressions (000's)",
    yAxisNamePadding: 10
  },
  categories: null,
  dataset: [
    {
      seriesName: '$ Revenue',
      data: null
    },
    {
      seriesName: 'Clicks',
      data: null
    },
    {
      seriesName: "Impressions (000's)",
      data: null
    }
  ]
}
