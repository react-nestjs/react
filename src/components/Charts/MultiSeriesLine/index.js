import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Rnd } from 'react-rnd'
import Loader from 'react-loader-spinner'
import ReactFusionCharts from 'react-fusioncharts'
import FusionCharts from 'fusioncharts/core'
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
import Msline from 'fusioncharts/viz/msline'
import moment from 'moment/moment'

import httpService from 'services/httpService'

import actions from 'enums/actions'
import ui from 'config/ui'
import urls from 'enums/urls'

ReactFusionCharts.fcRoot(FusionCharts, Msline, FusionTheme)

const MultiSeriesLineChart = props => {
  const { multiSeriesLineChart, setMultiSeriesLineChart } = { ...props }
  const [cradleLoaderState, setCradleLoaderState] = useState({ loading: false })
  const {
    chartContainer: { bounds, maxHeight, maxWidth, minHeight, minWidth, style }
  } = ui

  const setMultiSeriesStats = data => {
    const dates = data.map(obj => moment(obj.date.split('T')[0]))
    const { multiple } = multiSeriesLineChart
    const MULTIPLE = multiple

    let multiplier = 1

    const labels = [...Array(data.length)].map((el, i) => {
      if (i === MULTIPLE * multiplier) {
        multiplier += 2
        return { label: dates[i].format('MMM D') }
      }

      return { label: '' }
    })

    multiSeriesLineChart.categories = [{ category: labels }]

    multiSeriesLineChart.dataset[0].data = data.map(obj => ({ value: obj.revenue }))
    multiSeriesLineChart.dataset[1].data = data.map(obj => ({ value: obj.clicks }))
    multiSeriesLineChart.dataset[2].data = data.map(obj => ({ value: obj.impressions / 1000 }))

    multiSeriesLineChart.chart.subCaption = `${dates[0].format('MMMM D, YYYY')} - ${dates[
      dates.length - 1
    ].format('MMMM D, YYYY')}`

    setMultiSeriesLineChart(multiSeriesLineChart)
  }

  const toggleCradleLoaderState = () => {
    cradleLoaderState.loading = !cradleLoaderState.loading
    setCradleLoaderState(cradleLoaderState)
  }
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    const { categories } = multiSeriesLineChart

    const getData = url => {
      toggleCradleLoaderState()

      httpService
        .get(url)
        .then(({ data }) => {
          toggleCradleLoaderState()

          data
            ? data.length
              ? setMultiSeriesStats(data)
              : console.log(`No ${url} query data returned`)
            : console.log(`httpService Response Failed:\n`, data)
        })
        .catch(err => {
          console.log(`httpService Failed:\n`, err)
          toggleCradleLoaderState()
        })
    }

    if (!categories) {
      const {
        DATA: { STATS_HOURLY_ENDPOINT }
      } = urls

      getData(STATS_HOURLY_ENDPOINT)
    }
  }, [])

  useEffect(() => {
    const { categories } = multiSeriesLineChart

    if (!categories) {
      const { header, height, width } = multiSeriesLineChart
      const HEADER = header

      multiSeriesLineChart.x = window.innerWidth / 2 - width / 2
      multiSeriesLineChart.y = (window.innerHeight - HEADER) / 2 - height / 2
      setMultiSeriesLineChart(multiSeriesLineChart)
    }
  }, [])

  return (
    <Rnd
      bounds={bounds}
      maxHeight={maxHeight}
      maxWidth={maxWidth}
      minHeight={minHeight}
      minWidth={minWidth}
      style={style}
      position={{ x: multiSeriesLineChart.x, y: multiSeriesLineChart.y }}
      onDragStop={(evt, d) => {
        multiSeriesLineChart.x = d.x
        multiSeriesLineChart.y = d.y
        setMultiSeriesLineChart(multiSeriesLineChart)
      }}
      size={{ height: multiSeriesLineChart.height, width: multiSeriesLineChart.width }}
      onResize={(evt, direction, ref, delta, position) => {
        multiSeriesLineChart.height = ref.offsetHeight
        multiSeriesLineChart.width = ref.offsetWidth
        multiSeriesLineChart.x = position.x
        multiSeriesLineChart.y = position.y
        setMultiSeriesLineChart(multiSeriesLineChart)
      }}
    >
      {cradleLoaderState.loading && (
        <Loader type="CradleLoader" style={{ display: 'inline-block', paddingBottom: '500px' }} />
      )}
      {multiSeriesLineChart.categories && (
        <ReactFusionCharts
          type={multiSeriesLineChart.chart.type}
          dataFormat={multiSeriesLineChart.chart.dataFormat}
          dataSource={multiSeriesLineChart}
          height={multiSeriesLineChart.height - multiSeriesLineChart.padding}
          width={multiSeriesLineChart.width - multiSeriesLineChart.padding}
        />
      )}
    </Rnd>
  )
}

const mapStateToProps = state => {
  const { multiSeriesLineChart } = state
  return { multiSeriesLineChart }
}

const mapDispatchToProps = dispatch => {
  const { SET_MULTI_SERIES_LINE_CHART } = actions

  return {
    setMultiSeriesLineChart: settings => dispatch({ type: SET_MULTI_SERIES_LINE_CHART, settings })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MultiSeriesLineChart)
