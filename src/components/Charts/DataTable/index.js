import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { Rnd } from 'react-rnd'
import DataGrid from 'react-data-grid'
import FuzzySearch from 'fuzzy-search'
import { Popup } from 'mapbox-gl'
import moment from 'moment/moment'

import Header from './Header'

import geojsonService from 'services/geojsonService'
import httpService from 'services/httpService'

import actions from 'enums/actions'
import config from './index.config'
import ui from 'config/ui'
import urls from 'enums/urls'

const DataTable = props => {
  const { dataTable, setDataTable } = { ...props }
  const {
    tableContainer: { bounds, maxHeight, maxWidth, minHeight, minWidth, style }
  } = ui

  const resetMetricLayer = () => {
    const { map, metrics, popup } = dataTable

    if (popup) {
      /* fix for mapbox-gl bug that sometimes adds multiple popups on single mouse click */
      document.querySelectorAll('.mapboxgl-popup').forEach(el => (el.style.display = 'none'))
    }

    for (const key of metrics) {
      if (map.getSource(key) && map.isSourceLoaded(key)) {
        map.removeLayer(key)
        map.removeSource(key)
        break
      }
    }
  }

  const showDataTable = id => {
    const { inputElementValue, selectedIndexes } = dataTable
    const {
      DATA: {
        EVENTS_DAILY_POI_ENDPOINT,
        EVENTS_HOURLY_POI_ENDPOINT,
        STATS_DAILY_POI_ENDPOINT,
        STATS_HOURLY_POI_ENDPOINT
      }
    } = urls

    resetMetricLayer()

    if (inputElementValue) {
      dataTable.inputElementValue = ''
    }

    if (selectedIndexes.length) {
      dataTable.selectedIndexes = []
    }

    dataTable.id = id
    setDataTable(dataTable)

    switch (id) {
      case 'eventsDaily':
        return getData(EVENTS_DAILY_POI_ENDPOINT)

      case 'eventsHourly':
        return getData(EVENTS_HOURLY_POI_ENDPOINT)

      case 'statsDaily':
        return getData(STATS_DAILY_POI_ENDPOINT)

      case 'statsHourly':
        return getData(STATS_HOURLY_POI_ENDPOINT)

      default:
        throw new Error('default case not expected')
    }
  }

  const getData = url => {
    httpService
      .get(url)
      .then(({ data }) => {
        data
          ? data.length
            ? setData(data)
            : console.log(`No ${url} query data returned`)
          : console.log(`httpService Response Failed:\n`, data)
      })
      .catch(err => {
        console.log(`httpService Failed:\n`, err)
        /* rate limiter error response - max 10 requests per 30 seconds */
        if (err.response?.data) {
          return alert(err.response.data)
        }
      })
  }

  const setData = data => {
    const { columnsFilterKeys } = dataTable

    dataTable.columns = Reflect.ownKeys(data[0])
      .filter(key => !columnsFilterKeys.includes(key))
      .map(key => {
        switch (key) {
          case 'hour':
            return { key, name: 'Hourly Time' }

          case 'name':
            return { key, name: 'Point of Interest' }

          default:
            return { key, name: key.replace(/^\w/, char => char.toUpperCase()) }
        }
      })

    dataTable.rows = data.map(data => {
      const { id, lat, lon } = data
      const { columns } = dataTable

      let row = { id, lat, lon }

      columns.forEach(({ key }) => {
        if (key === 'date') {
          const { date } = data

          return (row = {
            ...row,
            [key]: moment(date.split('T')[0]).format('MMM D, YYYY')
          })
        }

        if (typeof data[key] === 'number') {
          const { minimumFractionDigits } = dataTable
          /* convert 24 hour time to 12 hour time am/pm */
          if (key === 'hour') {
            return (row = {
              ...row,
              [key]: moment(data[key].toString(), 'H').format('ha')
            })
          }
          /* number - no decimals */
          if (data[key].toString().indexOf('.') === -1) {
            return (row = {
              ...row,
              [key]: data[key].toLocaleString('en-CA')
            })
          }
          /* number - decimals */
          return (row = {
            ...row,
            [key]: data[key].toLocaleString('en-CA', { minimumFractionDigits })
          })
        }

        return (row = {
          ...row,
          [key]: data[key]
        })
      })

      return row
    })

    setDataTable(dataTable)
    setInputElement()
  }

  const setInputElement = () => {
    const { inputElementDisabled } = dataTable

    if (inputElementDisabled) {
      dataTable.inputElementDisabled = !inputElementDisabled
      dataTable.inputElement.disabled = dataTable.inputElementDisabled
      setDataTable(dataTable)
    }

    dataTable.inputElement.focus()
  }

  const changeInputValue = value => {
    dataTable.inputElementValue = value
    setDataTable(dataTable)

    if (value.length < 2) {
      const { selectedIndexes } = dataTable

      if (selectedIndexes.length) {
        dataTable.selectedIndexes = []
        setDataTable(dataTable)
      }

      return true
    }

    performFuzzySearch()
  }

  const performFuzzySearch = () => {
    const { inputElementValue, rows } = dataTable
    const fuzzySearch = new FuzzySearch(rows, ['name'])

    dataTable.selectedIndexes = fuzzySearch.search(inputElementValue).map(row => row.id - 1)
    setDataTable(dataTable)
  }

  const addMetricLayer = (i, j) => {
    const { columns, id, map, metrics, rows } = dataTable
    const { key } = columns[i]

    if (id !== 'statsDaily' || !metrics.includes(key)) {
      return true
    }

    resetMetricLayer()

    map.addSource(key, {
      type: 'geojson',
      data: geojsonService.setGeoJSON(
        rows.filter(row => row.date === rows[j].date),
        metrics
      ),
      cluster: true,
      clusterMaxZoom: 2,
      clusterRadius: 50
    })

    map.addLayer({
      id: key,
      source: key,
      type: 'circle',
      filter: ['has', key],
      paint: {
        'circle-stroke-color': '#000000',
        'circle-stroke-width': 1,
        'circle-color': [
          'step',
          ['get', key],
          '#669966',
          dataTable[key][0],
          '#3399ff',
          dataTable[key][1],
          '#ffcc99',
          dataTable[key][2],
          '#9966ff',
          dataTable[key][3],
          '#33cccc'
        ],
        'circle-radius': [
          'step',
          ['get', key],
          10,
          dataTable[key][0],
          20,
          dataTable[key][1],
          30,
          dataTable[key][2],
          40,
          dataTable[key][3],
          50
        ]
      }
    })

    addMetricPopup(key)
  }

  const addMetricPopup = key => {
    const { columns, map } = dataTable

    map
      .on('click', key, ({ features, lngLat }) => {
        const { minimumFractionDigits, popup } = dataTable
        const {
          properties: { clicks, date, impressions, name, revenue }
        } = features[0]

        if (popup) {
          /* fix for mapbox-gl bug that sometimes adds multiple popups on single mouse click */
          document.querySelectorAll('.mapboxgl-popup').forEach(el => (el.style.display = 'none'))
        }

        dataTable.popup = new Popup({
          closeButton: false
        })

        dataTable.popup
          .setLngLat(lngLat)
          .setHTML(
            `<p style="font-weight: bold; padding-bottom: 2px;">${name}: ${date}</p>
            <p>${columns[2].name}: ${impressions.toLocaleString('en-CA')}</p>
            <p>${columns[3].name}: ${clicks.toLocaleString('en-CA')}</p>
            <p>${columns[4].name}: ${revenue.toLocaleString('en-CA', {
              minimumFractionDigits
            })}</p>`
          )
          .addTo(map)

        setDataTable(dataTable)
      })
      .on('mouseenter', key, () => {
        map.getCanvas().style.cursor = 'pointer'
      })
      .on('mouseleave', key, () => {
        map.getCanvas().style.cursor = ''
      })
  }
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    const { height, width } = config
    const { header, inputElementDisabled } = dataTable
    const HEADER = header

    dataTable.inputElementValue = ''
    dataTable.inputElement = document.getElementById('input')
    dataTable.inputElement.disabled = inputElementDisabled

    dataTable.height = height
    dataTable.width = width

    dataTable.x = window.innerWidth / 2 - width / 2
    dataTable.y = (window.innerHeight - HEADER) / 2 - height / 2

    setDataTable(dataTable)
  }, [])

  const onButtonClickHandler = ({ target: { id } }) => showDataTable(id)
  const onCellSelectedHandler = ({ idx, rowIdx }) => addMetricLayer(idx, rowIdx)
  const onChangeInputValueHandler = ({ target: { value } }) => changeInputValue(value)

  return (
    <Rnd
      bounds={bounds}
      maxHeight={maxHeight}
      maxWidth={maxWidth}
      minHeight={minHeight}
      minWidth={minWidth}
      style={style}
      position={{ x: dataTable.x, y: dataTable.y }}
      onDragStop={(evt, d) => {
        if (dataTable.stopPropagation) {
          dataTable.stopPropagation = false
          return setDataTable(dataTable)
        }

        dataTable.x = d.x
        dataTable.y = d.y
        setDataTable(dataTable)
      }}
      size={{ height: dataTable.height, width: dataTable.width }}
      onResize={(evt, direction, ref, delta, position) => {
        dataTable.height = ref.offsetHeight
        dataTable.width = ref.offsetWidth
        dataTable.x = position.x
        dataTable.y = position.y
        setDataTable(dataTable)
      }}
    >
      <Header
        count={!dataTable.inputElementDisabled && dataTable.selectedIndexes.length.toString()}
        value={dataTable.inputElementValue}
        width={dataTable.width - dataTable.widthPadding + 2}
        onButtonClick={onButtonClickHandler}
        onChangeInputValue={onChangeInputValueHandler}
      />
      {dataTable.columns && dataTable.rows && (
        <div style={{ paddingBottom: '12px' }}>
          <DataGrid
            columns={dataTable.columns}
            rowGetter={i => dataTable.rows[i]}
            rowsCount={dataTable.rows.length}
            minHeight={dataTable.height - dataTable.heightPadding}
            minWidth={dataTable.width - dataTable.widthPadding}
            onCellSelected={onCellSelectedHandler}
            onScroll={() => {
              dataTable.stopPropagation = true
              setDataTable(dataTable)
            }}
            rowSelection={{
              showCheckbox: false,
              selectBy: {
                indexes: dataTable.selectedIndexes
              }
            }}
          />
        </div>
      )}
    </Rnd>
  )
}

const mapStateToProps = state => {
  const { dataTable } = state
  return { dataTable }
}

const mapDispatchToProps = dispatch => {
  const { SET_DATA_TABLE } = actions

  return {
    setDataTable: settings => dispatch({ type: SET_DATA_TABLE, settings })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DataTable)
