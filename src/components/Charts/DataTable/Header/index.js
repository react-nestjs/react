import React, { memo } from 'react'

import scss from './index.module.scss'

const Header = memo(({ count, value, width, onButtonClick, onChangeInputValue }) => {
  let rowsSelected = ''

  if (count) {
    rowsSelected = +count === 1 ? `${count} row selected` : `${count} rows selected`
  }

  return (
    <div className={scss.Header} style={{ width }}>
      <img src="assets/logo-small.png" alt="logo" />
      <p className={scss.title}>Point of Interest Fuzzy Search</p>
      <input id="input" type="text" value={value} onChange={onChangeInputValue} />
      <p className={scss.count}>{rowsSelected}</p>
      <button id="eventsDaily" className={scss.eventsDaily} onClick={onButtonClick}>
        Daily POI Events
      </button>
      <button id="eventsHourly" className={scss.eventsHourly} onClick={onButtonClick}>
        Hourly POI Events
      </button>
      <button id="statsDaily" className={scss.statsDaily} onClick={onButtonClick}>
        Daily POI Statistics
      </button>
      <button id="statsHourly" className={scss.statsHourly} onClick={onButtonClick}>
        Hourly POI Statistics
      </button>
    </div>
  )
})

export default Header
