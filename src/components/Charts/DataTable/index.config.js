export default {
  header: 45,
  heightPadding: 105,
  widthPadding: 30,
  height: 560,
  width: 980,
  x: 0,
  y: 0,
  zoom: 12,
  columns: null,
  columnsFilterKeys: ['id', 'lat', 'lon'],
  inputElement: null,
  inputElementDisabled: true,
  inputElementValue: '',
  map: null,
  metrics: ['clicks', 'impressions', 'revenue'],
  minimumFractionDigits: 6,
  popup: null,
  rows: null,
  selectedIndexes: [],
  clicks: [200, 400, 600, 800],
  impressions: [100000, 200000, 300000, 400000],
  revenue: [400, 800, 1200, 1600]
}
