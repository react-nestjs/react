import React, { memo } from 'react'

import ColumnChart from './Column'
import DataTable from './DataTable'
import GroupedColumnChart from './GroupedColumn'
import MultiSeriesLineChart from './MultiSeriesLine'

const Charts = memo(props => {
  return (
    <>
      {props.showColumnChart && <ColumnChart showColumnState={props.showColumnChart} />}
      {props.showGroupedColumnChart && (
        <GroupedColumnChart showGroupedColumnState={props.showGroupedColumnChart} />
      )}
      {props.showMultiSeriesLineChart && (
        <MultiSeriesLineChart showMultiSeriesLineState={props.showMultiSeriesLineChart} />
      )}
      {props.showDataTable && <DataTable showDataTableState={props.showDataTable} />}
    </>
  )
})

export default Charts
