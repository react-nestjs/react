import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Rnd } from 'react-rnd'
import Loader from 'react-loader-spinner'
import ReactFusionCharts from 'react-fusioncharts'
import FusionCharts from 'fusioncharts/core'
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
import Mscolumn3D from 'fusioncharts/viz/mscolumn3d'
import moment from 'moment/moment'

import httpService from 'services/httpService'

import actions from 'enums/actions'
import ui from 'config/ui'
import urls from 'enums/urls'

ReactFusionCharts.fcRoot(FusionCharts, Mscolumn3D, FusionTheme)

const GroupedColumnChart = props => {
  const { groupedColumnChart, setGroupedColumnChart } = { ...props }
  const [cradleLoaderState, setCradleLoaderState] = useState({ loading: false })
  const {
    chartContainer: { bounds, maxHeight, maxWidth, minHeight, minWidth, style }
  } = ui

  const setGroupedColumnStats = data => {
    const dates = data.map(obj => moment(obj.date.split('T')[0]))
    const labels = data.map((obj, i) => ({ label: dates[i].format('MMM D') }))

    groupedColumnChart.categories = [{ category: labels }]

    groupedColumnChart.dataset[0].data = data.map(obj => ({
      value: obj.revenue / (obj.impressions / 1000)
    }))
    groupedColumnChart.dataset[1].data = data.map(obj => ({ value: obj.revenue / obj.clicks }))

    groupedColumnChart.chart.subCaption = `${dates[0].format('MMMM D, YYYY')} - ${dates[
      dates.length - 1
    ].format('MMMM D, YYYY')}`

    setGroupedColumnChart(groupedColumnChart)
  }

  const toggleCradleLoaderState = () => {
    cradleLoaderState.loading = !cradleLoaderState.loading
    setCradleLoaderState(cradleLoaderState)
  }
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    const { categories } = groupedColumnChart

    const getData = url => {
      toggleCradleLoaderState()

      httpService
        .get(url)
        .then(({ data }) => {
          toggleCradleLoaderState()

          data
            ? data.length
              ? setGroupedColumnStats(data)
              : console.log(`No ${url} query data returned`)
            : console.log(`httpService Response Failed:\n`, data)
        })
        .catch(err => {
          console.log(`httpService Failed:\n`, err)
          toggleCradleLoaderState()
        })
    }

    if (!categories) {
      const {
        DATA: { STATS_DAILY_ENDPOINT }
      } = urls

      getData(STATS_DAILY_ENDPOINT)
    }
  }, [])

  useEffect(() => {
    const { categories } = groupedColumnChart

    if (!categories) {
      const { header, height, width } = groupedColumnChart
      const HEADER = header

      groupedColumnChart.x = window.innerWidth / 2 - width / 2
      groupedColumnChart.y = (window.innerHeight - HEADER) / 2 - height / 2
      setGroupedColumnChart(groupedColumnChart)
    }
  }, [])

  return (
    <Rnd
      bounds={bounds}
      maxHeight={maxHeight}
      maxWidth={maxWidth}
      minHeight={minHeight}
      minWidth={minWidth}
      style={style}
      position={{ x: groupedColumnChart.x, y: groupedColumnChart.y }}
      onDragStop={(evt, d) => {
        groupedColumnChart.x = d.x
        groupedColumnChart.y = d.y
        setGroupedColumnChart(groupedColumnChart)
      }}
      size={{ height: groupedColumnChart.height, width: groupedColumnChart.width }}
      onResize={(evt, direction, ref, delta, position) => {
        groupedColumnChart.height = ref.offsetHeight
        groupedColumnChart.width = ref.offsetWidth
        groupedColumnChart.x = position.x
        groupedColumnChart.y = position.y
        setGroupedColumnChart(groupedColumnChart)
      }}
    >
      {cradleLoaderState.loading && (
        <Loader type="CradleLoader" style={{ display: 'inline-block', paddingBottom: '500px' }} />
      )}
      {groupedColumnChart.categories && (
        <ReactFusionCharts
          type={groupedColumnChart.chart.type}
          dataFormat={groupedColumnChart.chart.dataFormat}
          dataSource={groupedColumnChart}
          height={groupedColumnChart.height - groupedColumnChart.padding}
          width={groupedColumnChart.width - groupedColumnChart.padding}
        />
      )}
    </Rnd>
  )
}

const mapStateToProps = state => {
  const { groupedColumnChart } = state
  return { groupedColumnChart }
}

const mapDispatchToProps = dispatch => {
  const { SET_GROUPED_COLUMN_CHART } = actions

  return {
    setGroupedColumnChart: settings => dispatch({ type: SET_GROUPED_COLUMN_CHART, settings })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupedColumnChart)
