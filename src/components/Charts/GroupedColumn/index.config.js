export default {
  header: 45,
  padding: 30,
  height: 500,
  width: 750,
  x: 0,
  y: 0,
  chart: {
    type: 'mscolumn3d',
    theme: 'fusion',
    baseFont: 'Roboto',
    borderColor: '#8c8278',
    caption: 'Daily Revenue: per 1000 Impressions & per Click',
    captionFontSize: 20,
    captionPadding: 20,
    chartBottomMargin: 10,
    chartRightMargin: 30,
    chartTopMargin: 25,
    dataFormat: 'json',
    decimals: '2',
    forceDecimals: '1',
    forceYAxisValueDecimals: '0',
    formatNumber: '1',
    logoPosition: 'tl',
    logoUrl: 'assets/logo-small.png',
    logoLeftMargin: 11,
    logoTopMargin: 10,
    numberPrefix: '$',
    plotBorderAlpha: 40,
    plotBorderColor: '#000000',
    plotHoverEffect: '0',
    plotToolText: '<b>$dataValue $seriesName</b>',
    showBorder: '1',
    showPlotBorder: '1',
    subCaption: '',
    subCaptionFontSize: 16,
    yAxisName: '$ Revenue',
    yAxisNamePadding: 10
  },
  categories: null,
  dataset: [
    {
      seriesName: 'Revenue per 1000 Impressions',
      data: null
    },
    {
      seriesName: 'Revenue per Click',
      data: null
    }
  ]
}
