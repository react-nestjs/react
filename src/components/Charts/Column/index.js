import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import { Rnd } from 'react-rnd'
import Loader from 'react-loader-spinner'
import ReactFusionCharts from 'react-fusioncharts'
import FusionCharts from 'fusioncharts/core'
import FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion'
import Column3D from 'fusioncharts/viz/column3d'
import moment from 'moment/moment'

import httpService from 'services/httpService'

import linkedDataConfig from './linked-data.config'

import actions from 'enums/actions'
import ui from 'config/ui'
import urls from 'enums/urls'

ReactFusionCharts.fcRoot(FusionCharts, Column3D, FusionTheme)

const ColumnChart = props => {
  const { columnChart, setColumnChart } = { ...props }
  const [cradleLoaderState, setCradleLoaderState] = useState({ loading: false })
  const {
    chartContainer: { bounds, maxHeight, maxWidth, minHeight, minWidth, style }
  } = ui

  const setEventsDaily = data => {
    const { colors } = columnChart

    columnChart.dates = data.map(obj => moment(obj.date.split('T')[0]))

    columnChart.data = data.map((obj, i) => ({
      color: colors[i],
      label: columnChart.dates[i].format('MMM D'),
      link: `newchart-xml-${columnChart.dates[i].format('MMM D')}`,
      value: obj.events
    }))

    columnChart.chart.caption = `Daily Events: ${columnChart.dates[0].format(
      'MMMM D, YYYY'
    )} - ${columnChart.dates[columnChart.dates.length - 1].format('MMMM D, YYYY')}`

    setColumnChart(columnChart)
  }

  const setEventsHourly = data => {
    const { colors, dates, yAxisMaxValue } = columnChart

    columnChart.linkedData = dates.map((date, i) => {
      const events = data.filter(
        obj => moment(obj.date.split('T')[0]).format('MMM D') === date.format('MMM D')
      )

      const linkedData = JSON.parse(JSON.stringify(linkedDataConfig))
      linkedData.id = date.format('MMM D')
      linkedData.linkedChart.chart.caption = `${date.format('MMMM D, YYYY')} Hourly Events`
      linkedData.linkedChart.data = [...Array(data.length / dates.length)] // 24 hours
        .map((obj, j) =>
          j % 2 === 0
            ? (obj = {
                color: colors[i],
                /* convert 24 hour time to 12 hour time am/pm */
                label: moment(j.toString(), 'H').format('ha'),
                value: 0
              })
            : (obj = { color: colors[i], label: '', value: 0 })
        )
        .map((obj, j) => {
          for (const event of events) {
            if (event.hour === j) {
              obj.value = event.events
              break
            }
          }

          return obj
        })

      if (i === 0) {
        linkedData.linkedChart.chart.yAxisMaxValue = yAxisMaxValue
      }

      return linkedData
    })

    setColumnChart(columnChart)
  }

  const toggleCradleLoaderState = () => {
    cradleLoaderState.loading = !cradleLoaderState.loading
    setCradleLoaderState(cradleLoaderState)
  }
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    const { data } = columnChart

    const getData = url => {
      toggleCradleLoaderState()

      httpService
        .get(url)
        .then(({ data }) => {
          toggleCradleLoaderState()

          if (data) {
            return data
          }

          console.log(`httpService Response Failed:\n`, data)
        })
        .then(data => {
          if (data.length) {
            const {
              DATA: { EVENTS_DAILY_ENDPOINT, EVENTS_HOURLY_ENDPOINT }
            } = urls

            switch (url) {
              case EVENTS_DAILY_ENDPOINT:
                setEventsDaily(data)
                return getData(EVENTS_HOURLY_ENDPOINT)

              case EVENTS_HOURLY_ENDPOINT:
                return setEventsHourly(data)

              default:
                throw new Error('default case not expected')
            }
          }

          console.log(`No ${url} query data returned`)
        })
        .catch(err => {
          console.log(`httpService Failed:\n`, err)
          toggleCradleLoaderState()
        })
    }

    if (!data) {
      const {
        DATA: { EVENTS_DAILY_ENDPOINT }
      } = urls

      getData(EVENTS_DAILY_ENDPOINT)
    }
  }, [])

  useEffect(() => {
    const { data } = columnChart

    if (!data) {
      const { header, height, width } = columnChart
      const HEADER = header

      columnChart.x = window.innerWidth / 2 - width / 2
      columnChart.y = (window.innerHeight - HEADER) / 2 - height / 2
      setColumnChart(columnChart)
    }
  }, [])

  return (
    <Rnd
      bounds={bounds}
      maxHeight={maxHeight}
      maxWidth={maxWidth}
      minHeight={minHeight}
      minWidth={minWidth}
      style={style}
      position={{ x: columnChart.x, y: columnChart.y }}
      onDragStop={(evt, d) => {
        columnChart.x = d.x
        columnChart.y = d.y
        setColumnChart(columnChart)
      }}
      size={{ height: columnChart.height, width: columnChart.width }}
      onResize={(evt, direction, ref, delta, position) => {
        columnChart.height = ref.offsetHeight
        columnChart.width = ref.offsetWidth
        columnChart.x = position.x
        columnChart.y = position.y
        setColumnChart(columnChart)
      }}
    >
      {cradleLoaderState.loading && (
        <Loader type="CradleLoader" style={{ display: 'inline-block', paddingBottom: '500px' }} />
      )}
      {columnChart.data && (
        <ReactFusionCharts
          type={columnChart.chart.type}
          dataFormat={columnChart.chart.dataFormat}
          dataSource={columnChart}
          height={columnChart.height - columnChart.padding}
          width={columnChart.width - columnChart.padding}
        />
      )}
    </Rnd>
  )
}

const mapStateToProps = state => {
  const { columnChart } = state
  return { columnChart }
}

const mapDispatchToProps = dispatch => {
  const { SET_COLUMN_CHART } = actions

  return {
    setColumnChart: settings => dispatch({ type: SET_COLUMN_CHART, settings })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ColumnChart)
