export default {
  id: '',
  linkedChart: {
    chart: {
      baseFont: 'Roboto',
      borderColor: '#8c8278',
      caption: '',
      captionFontSize: 20,
      captionPadding: 25,
      chartBottomMargin: 35,
      chartRightMargin: 30,
      chartTopMargin: 25,
      labelStep: 2,
      logoPosition: 'tl',
      logoUrl: 'assets/logo-small.png',
      logoLeftMargin: 11,
      logoTopMargin: 10,
      plotBorderAlpha: 40,
      plotBorderColor: '#000000',
      plotHoverEffect: '0',
      plotToolText: '<b>$dataValue events</b>',
      rotateLabels: '0',
      showBorder: '1',
      showPlotBorder: '1',
      theme: 'fusion',
      yAxisName: '# of Events',
      yAxisNamePadding: 10
    },
    data: null
  }
}
