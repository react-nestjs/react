import React, { memo } from 'react'

import scss from './index.module.scss'

const Hexagon = memo(props => (
  <div className={scss.Hexagon}>
    <div className={scss.spacer}></div>
    <p className={scss.header}>UK Road Accidents</p>
    <div className={scss.spacer}></div>
    <div>
      <label>Coverage</label>
      <input
        id="coverage"
        type="range"
        min="0"
        max="1"
        step="0.1"
        value={props.coverage}
        onChange={props.onChangeInputValue}
      />
      <span className="coverage">{props.coverage}</span>
    </div>
    <div className={scss.spacer}></div>
    <div>
      <label>Elevation Scale</label>
      <input
        id="elevationScale"
        type="range"
        min="0"
        max="100"
        step="10"
        value={props.elevationScale}
        onChange={props.onChangeInputValue}
      />
      <span className="elevationScale">{props.elevationScale}</span>
    </div>
    <div className={scss.spacer}></div>
    <div>
      <label>Radius</label>
      <input
        id="radius"
        type="range"
        min="1000"
        max="5000"
        step="500"
        value={props.radius}
        onChange={props.onChangeInputValue}
      />
      <span className="radius">{props.radius}</span>
    </div>
    <div className={scss.spacer}></div>
    <div>
      <label>Upper Percentile</label>
      <input
        id="upperPercentile"
        type="range"
        min="80"
        max="100"
        step="1"
        value={props.upperPercentile}
        onChange={props.onChangeInputValue}
      />
      <span className="upperPercentile">{props.upperPercentile}</span>
    </div>
    <div className={scss.spacer}></div>
    <button onClick={props.resetHexagonSettings}>Reset Coordinates</button>
    <button onClick={props.resetHexagonParams}>Reset Parameters</button>
    <div className={scss.spacer}></div>
    <button onClick={props.returnToTrails}>Return to Trails</button>
    <button onClick={props.logout}>Logout</button>
    <div className={scss.spacer}></div>
  </div>
))

export default Hexagon
