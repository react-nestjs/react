import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'

import authenticationService from 'services/authenticationService'

import actions from 'enums/actions'
import scss from './index.module.scss'
import ui from 'config/ui'

const Login = props => {
  const {
    user: { username, password }
  } = ui
  const [userState] = useState({ username, password })
  const { splashScreen, setSplashScreenActive } = props

  const onSubmitHandler = evt => {
    evt.preventDefault()
    /* return authenticationService.register(props, { */
    authenticationService.login(props, {
      username: userState.username,
      password: userState.password
    })

    setSplashScreenActive()
  }
  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    if (splashScreen.active) {
      setSplashScreenActive()
    }
  }, [])

  return (
    <div className={scss.Login}>
      <form onSubmit={onSubmitHandler}>
        <fieldset className={scss.login}>
          <p className={scss.header}>Welcome!</p>
          <div className={scss.spacer}></div>
          <label htmlFor="username">Username:</label>
          <input name="username" type="email" value={userState.username} readOnly />
          <div className={scss.spacer}></div>
          <label htmlFor="password">Password:</label>
          <input name="password" type="text" value={userState.password} readOnly />
          <div className={scss.spacer}></div>
          <button type="submit">Login</button>
        </fieldset>
      </form>
    </div>
  )
}

const mapStateToProps = state => {
  const { splashScreen } = state
  return { splashScreen }
}

const mapDispatchToProps = dispatch => {
  const { SET_SPLASHSCREEN_ACTIVE } = actions

  return {
    setSplashScreenActive: () => dispatch({ type: SET_SPLASHSCREEN_ACTIVE })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)
