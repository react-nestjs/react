import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { compose } from 'redux'

import Charts from '../Charts'
import ChartElement from './ChartElement'

import authorizationService from 'services/authorizationService'

import actions from 'enums/actions'
import scss from './index.module.scss'

const Chart = props => {
  const { chartElements, setChartElements } = { ...props }
  const { dataTable, setDataTable } = { ...props }
  const [columnChartState, setColumnChartState] = useState({ showColumnChart: false })
  const [dataTableState, setDataTableState] = useState({ showDataTable: false })
  const [groupedColumnChartState, setGroupedColumnChartState] = useState({
    showGroupedColumnChart: false
  })
  const [multiSeriesLineChartState, setMultiSeriesLineChartState] = useState({
    showMultiSeriesLineChart: false
  })
  const [chartElementsHash] = useState(() =>
    Object.assign({}, ...chartElements.map((el, i) => ({ [el.id]: i })))
  )

  const resetDataTable = () => {
    const { columns, inputElementDisabled, map, metrics, popup, rows } = dataTable

    if (popup) {
      /* fix for mapbox-gl bug that sometimes adds multiple popups on single mouse click */
      document.querySelectorAll('.mapboxgl-popup').forEach(el => (el.style.display = 'none'))
    }

    if (columns && rows) {
      dataTable.inputElementDisabled = !inputElementDisabled
      dataTable.columns = null
      dataTable.rows = null
      setDataTable(dataTable)
    }

    for (const key of metrics) {
      if (map.getSource(key) && map.isSourceLoaded(key)) {
        map.removeLayer(key)
        map.removeSource(key)
        break
      }
    }
  }

  const showChart = id => {
    switch (id) {
      case 'columnChart':
        setColumnChartState(prevState => {
          return { ...prevState, showColumnChart: !prevState.showColumnChart }
        })

        break

      case 'dataTable':
        resetDataTable()
        setDataTableState(prevState => {
          return { ...prevState, showDataTable: !prevState.showDataTable }
        })

        break

      case 'groupedColumnChart':
        setGroupedColumnChartState(prevState => {
          return { ...prevState, showGroupedColumnChart: !prevState.showGroupedColumnChart }
        })

        break

      case 'multiSeriesLineChart':
        setMultiSeriesLineChartState(prevState => {
          return { ...prevState, showMultiSeriesLineChart: !prevState.showMultiSeriesLineChart }
        })

        break

      case 'trails':
        return props.history.push('/map')

      case 'logout':
        return authorizationService.logout()

      default:
        throw new Error('default case not expected')
    }

    setChartElementsState(id)
  }

  const setChartElementsState = id => {
    chartElements[chartElementsHash[id]].active = !chartElements[chartElementsHash[id]].active
    chartElements[chartElementsHash[id]].active
      ? (chartElements[chartElementsHash[id]].class = 'active')
      : (chartElements[chartElementsHash[id]].class = '')

    setChartElements(chartElements)
  }

  const showChartHandler = ({ target: { id } }) => showChart(id)

  return (
    <>
      <ul className={scss.ChartElement}>
        {chartElements.map(el => (
          <ChartElement
            class={scss[el.class]}
            click={showChartHandler}
            id={el.id}
            key={el.id}
            name={el.name}
          />
        ))}
      </ul>
      <Charts
        showColumnChart={columnChartState.showColumnChart}
        showDataTable={dataTableState.showDataTable}
        showGroupedColumnChart={groupedColumnChartState.showGroupedColumnChart}
        showMultiSeriesLineChart={multiSeriesLineChartState.showMultiSeriesLineChart}
      />
    </>
  )
}

const mapStateToProps = state => {
  const { chartElements, dataTable } = state
  return { chartElements, dataTable }
}

const mapDispatchToProps = dispatch => {
  const { SET_CHART_ELEMENTS, SET_DATA_TABLE } = actions

  return {
    setChartElements: settings => dispatch({ type: SET_CHART_ELEMENTS, settings }),
    setDataTable: settings => dispatch({ type: SET_DATA_TABLE, settings })
  }
}

export default compose(withRouter, connect(mapStateToProps, mapDispatchToProps))(Chart)
