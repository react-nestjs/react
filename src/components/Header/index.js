import React from 'react'

import scss from './index.module.scss'

const Header = () => (
  <header className={scss.Header}>
    <img className={scss.logo} src="assets/logo.png" alt="Geospatial Web" />
    <div className={scss.name}>Geospatial Web</div>
    <div className={scss.title}>
      NestJS | React | Redux | Esri ArcGIS 4 | Mapbox GL | Deck.GL | PostGIS 3 | Docker
    </div>
    <a
      className={scss.repo}
      href="https://gitlab.com/react-nestjs"
      rel="noopener noreferrer"
      target="_blank"
    >
      GitLab Repository
    </a>
  </header>
)

export default Header
