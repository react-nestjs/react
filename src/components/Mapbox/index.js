import React from 'react'

import scss from './index.module.scss'

const Mapbox = () => <div id="mapbox" className={scss.Mapbox}></div>

export default Mapbox
