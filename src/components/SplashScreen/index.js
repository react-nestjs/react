import React, { memo } from 'react'
import { connect } from 'react-redux'

import scss from './index.module.scss'

const SplashScreen = memo(props => (
  <div className={`${scss.SplashScreen} ${scss[props.splashScreen.class]}`}></div>
))

const mapStateToProps = state => {
  const { splashScreen } = state
  return { splashScreen }
}

export default connect(mapStateToProps)(SplashScreen)
