import React, { useEffect, useRef } from 'react'
import { connect } from 'react-redux'

import mapService from 'services/mapService'

import actions from 'enums/actions'
import scss from './index.module.scss'
import ui from 'config/ui'

const Trails = props => {
  const selectRef = useRef()
  const { trails } = ui
  const {
    trail: { name },
    setTrailName
  } = { ...props }

  const onSelectTrailHandler = () => {
    const {
      current: { value }
    } = selectRef

    const i = trails.findIndex(trail => trail.name === value)

    if (i > 0) {
      mapService.goTo(trails[i])
    }

    setTrailName(value)
  }

  useEffect(() => {
    const {
      current: { value }
    } = selectRef

    if (value !== name) {
      selectRef.current.value = name
    }
  }, [name])

  return (
    <select id="trails" ref={selectRef} className={scss.trails} onChange={onSelectTrailHandler}>
      {trails.map(trail => (
        <option key={trail.name}>{trail.name}</option>
      ))}
    </select>
  )
}

const mapStateToProps = state => {
  const { trail } = state
  return { trail }
}

const mapDispatchToProps = dispatch => {
  const { SET_TRAIL_NAME } = actions

  return {
    setTrailName: name => dispatch({ type: SET_TRAIL_NAME, name })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Trails)
