import React, { memo } from 'react'

const LayerIcon = memo(props => (
  <img
    alt={props.alt}
    className={props.class}
    height={props.height}
    id={props.id}
    onClick={props.click}
    src={props.src}
    width={props.width}
  />
))

export default LayerIcon
