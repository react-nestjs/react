import React, { memo } from 'react'

const LayerElement = memo(props => (
  <li>
    <div id={props.id} className={props.class} onClick={props.click}>
      {props.name}
    </div>
  </li>
))

export default LayerElement
