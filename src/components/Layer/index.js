import React, { useState } from 'react'
import { withRouter } from 'react-router-dom'

import LayerElement from './LayerElement'
import LayerIcon from './LayerIcon'

import authorizationService from 'services/authorizationService'
import graphicsLayerService from 'services/graphicsLayerService'
import mapService from 'services/mapService'

import scss from './index.module.scss'
import ui from 'config/ui'

const Layer = props => {
  const { layerElements, layerIcons } = ui
  const [layerElementsState, setLayerElementsState] = useState(layerElements)
  const [layerElementsHash] = useState(() =>
    Object.assign({}, ...layerElements.map((el, i) => ({ [el.id]: i })))
  )

  const displayLayer = id => {
    switch (id) {
      case 'biosphere':
      case 'office':
      case 'places':
      case 'trails':
      case 'trails-point':
        const gl = graphicsLayerService.graphicsLayers[graphicsLayerService.graphicsLayersHash[id]]

        graphicsLayerService.setGraphicsLayerVisibility(id)
        gl.visible ? mapService.addGraphicsLayer(gl) : mapService.removeGraphicsLayer(gl)

        if (id !== 'trails-point') {
          updateLayerElementsState(id)
        }

        break

      case 'hexagon':
        return props.history.push('/hexagon')

      case 'charts':
        return props.history.push('/charts')

      case 'logout':
        return authorizationService.logout()

      default:
        throw new Error('default case not expected')
    }
  }

  const updateLayerElementsState = id => {
    const layerElements = [...layerElementsState]

    layerElements[layerElementsHash[id]].active = !layerElements[layerElementsHash[id]].active
    layerElements[layerElementsHash[id]].active
      ? (layerElements[layerElementsHash[id]].class = 'active')
      : (layerElements[layerElementsHash[id]].class = '')

    setLayerElementsState(layerElements)
  }

  const displayLayerHandler = ({ target: { id } }) => {
    displayLayer(id)

    if (id === 'trails') {
      displayLayer(`${id}-point`)
    }
  }

  return (
    <ul className={scss.LayerElement}>
      {layerElements.map(el => (
        <LayerElement
          class={scss[el.class]}
          click={displayLayerHandler}
          id={el.id}
          key={el.id}
          name={el.name}
        />
      ))}
      <div className={scss.layerIcon}>
        {layerIcons.map(icon => (
          <LayerIcon
            alt={icon.name}
            class={scss[icon.id]}
            click={displayLayerHandler}
            height={icon.height}
            id={icon.id}
            key={icon.id}
            src={icon.src}
            width={icon.width}
          />
        ))}
      </div>
    </ul>
  )
}

export default withRouter(Layer)
