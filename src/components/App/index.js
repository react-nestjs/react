import React from 'react'
import { Route, Redirect, Switch } from 'react-router-dom'

import DataCharts from 'containers/DataCharts'
import Header from '../Header'
import Hexagon from 'containers/Hexagon'
import Login from '../Login'
import Map from 'containers/Map'
import SplashScreen from '../SplashScreen'

const App = () => (
  <>
    <Header />
    <SplashScreen />
    <Switch>
      <Route path="/" exact component={Login} />
      <Route path="/charts" component={DataCharts} />
      <Route path="/hexagon" component={Hexagon} />
      <Route path="/map" component={Map} />
      <Redirect to="/" />
    </Switch>
  </>
)

export default App
