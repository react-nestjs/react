export default {
  API: {
    GEOJSON_ENDPOINT: '/api/geojson',
    MAPBOX_ACCESS_TOKEN_ENDPOINT: '/api/mapbox-access-token'
  },
  AUTH: {
    LOGIN_ENDPOINT: '/auth/login',
    REGISTER_ENDPOINT: '/auth/register'
  },
  DATA: {
    EVENTS_DAILY_ENDPOINT: '/data/events/daily',
    EVENTS_DAILY_POI_ENDPOINT: '/data/events/daily/poi',
    EVENTS_HOURLY_ENDPOINT: '/data/events/hourly',
    EVENTS_HOURLY_POI_ENDPOINT: '/data/events/hourly/poi',
    STATS_DAILY_ENDPOINT: '/data/stats/daily',
    STATS_DAILY_POI_ENDPOINT: '/data/stats/daily/poi',
    STATS_HOURLY_ENDPOINT: '/data/stats/hourly',
    STATS_HOURLY_POI_ENDPOINT: '/data/stats/hourly/poi'
  },
  HEXAGON: {
    HEXAGON_DATA_URL:
      'https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-heatmap/heatmap-data.csv'
  }
}
