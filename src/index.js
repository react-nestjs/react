import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import 'index.css'
import 'styles/react-spinner-loader/react-spinner-loader.css'

import App from 'components/App'
import initialState from 'config/state'
import reducer from 'store/reducer'
import * as serviceWorker from 'serviceWorker'

const rootElement = document.getElementById('root')
const store = createStore(reducer, initialState)

render(
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>,
  rootElement
)

serviceWorker.unregister()
