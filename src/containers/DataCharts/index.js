import React, { Component } from 'react'
import { connect } from 'react-redux'
import mapboxgl from 'mapbox-gl'

import Chart from 'components/Chart'
import Mapbox from 'components/Mapbox'

import authorizationService from 'services/authorizationService'

import actions from 'enums/actions'
import scss from './index.module.scss'
import ui from 'config/ui'

class DataCharts extends Component {
  map = null
  ui = ui

  componentDidMount() {
    const { accessToken } = mapboxgl
    const { chartElements, setChartElements } = { ...this.props }

    setChartElements(
      chartElements.map(el => {
        el.active = false
        el.class = ''
        return el
      })
    )

    accessToken && !authorizationService.hasSessionExpired()
      ? this.loadMap()
      : window.location.replace('/')
  }

  componentWillUnmount() {
    const { setSplashScreenActive } = this.props

    setSplashScreenActive()
    this.map.off('load')
  }

  loadMap() {
    const { dataTable, setDataTable } = { ...this.props }

    const {
      map: {
        navigationControl: { position },
        settings
      }
    } = this.ui

    this.map = new mapboxgl.Map(settings)
      .addControl(new mapboxgl.NavigationControl(), position)
      .on('load', () => {
        this.setSplashScreenActive()
      })

    dataTable.map = this.map
    setDataTable(dataTable)
  }

  setSplashScreenActive = () => {
    const { splashScreen, setSplashScreenActive } = this.props

    if (splashScreen.active) {
      setSplashScreenActive()
    }
  }

  render() {
    return (
      <div className={scss.DataCharts}>
        <Mapbox />
        <Chart />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { chartElements, dataTable, splashScreen } = state
  return { chartElements, dataTable, splashScreen }
}

const mapDispatchToProps = dispatch => {
  const { SET_CHART_ELEMENTS, SET_DATA_TABLE, SET_SPLASHSCREEN_ACTIVE } = actions

  return {
    setChartElements: settings => dispatch({ type: SET_CHART_ELEMENTS, settings }),
    setDataTable: settings => dispatch({ type: SET_DATA_TABLE, settings }),
    setSplashScreenActive: () => dispatch({ type: SET_SPLASHSCREEN_ACTIVE })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DataCharts)
