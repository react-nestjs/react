import React, { Component } from 'react'
import { connect } from 'react-redux'
import mapboxgl from 'mapbox-gl'

import Layer from 'components/Layer'
import Trails from 'components/Trails'

import authorizationService from 'services/authorizationService'
import esriService from 'services/esriService'
import mapService from 'services/mapService'

import actions from 'enums/actions'
import scss from './index.module.scss'
import ui from 'config/ui'

class Map extends Component {
  basemapToggle = null
  basemapToggleHandler = null
  map = null
  ui = ui
  view = null
  viewPointerMoveHandler = null

  componentDidMount() {
    const { accessToken } = mapboxgl

    accessToken && !authorizationService.hasSessionExpired()
      ? this.loadMap()
      : window.location.replace('/')
  }

  componentWillUnmount() {
    const { setSplashScreenActive } = this.props

    setSplashScreenActive()

    this.basemapToggleHandler.remove()
    this.viewPointerMoveHandler.remove()
  }

  loadMap() {
    const {
      view: { basemap }
    } = this.props

    this.map = new esriService.Map({
      basemap: basemap.active
    })

    mapService.map = this.map
    this.setMapView()
  }

  setMapView() {
    const {
      view: {
        settings: { center, zoom }
      }
    } = this.props

    const {
      view: { constraints, container, popup }
    } = this.ui

    this.view = new esriService.MapView({
      center,
      container,
      map: this.map,
      popup,
      zoom
    })

    this.view.constraints = constraints

    mapService.view = this.view
    this.loadMapView()
  }

  loadMapView() {
    this.view.when(() => {
      esriService.watchUtils.whenTrue(this.view, 'stationary', () => this.setViewSettings())

      this.setBasemapToggle()
      this.view.ui.add(this.basemapToggle, 'top-right')

      this.viewPointerMoveHandler = this.view.on('pointer-move', evt => {
        this.view.hitTest(evt).then(({ results }) => {
          const { popup } = this.view

          if (results.length && results[0].graphic.geometry.type === 'point') {
            const {
              mapPoint,
              graphic: {
                attributes: { description, name }
              }
            } = results[0]

            return popup.open({
              location: mapPoint,
              title: name,
              content: description
            })
          }

          if (popup.visible) {
            return popup.close()
          }

          return true
        })
      })

      setTimeout(() => {
        const { splashScreen, setSplashScreenActive } = this.props

        if (splashScreen.active) {
          setSplashScreenActive()
        }

        mapService.addGraphicsLayers()
      }, 500)
    })
  }

  setBasemapToggle() {
    const {
      view: { basemap },
      setBasemapActive
    } = this.props

    this.basemapToggle = new esriService.BasemapToggle({
      view: this.view,
      nextBasemap: basemap.next
    })

    this.basemapToggleHandler = this.basemapToggle.on('toggle', () => setBasemapActive())
  }

  setViewSettings() {
    const {
      view: { settings },
      setViewSettings
    } = { ...this.props }

    const {
      center: { longitude, latitude },
      zoom
    } = this.view

    settings.center = [longitude, latitude]
    settings.zoom = zoom

    setViewSettings(settings)
  }

  render() {
    return (
      <div id="map" className={scss.Map}>
        <Layer />
        <Trails />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { splashScreen, view } = state
  return { splashScreen, view }
}

const mapDispatchToProps = dispatch => {
  const { SET_BASEMAP_ACTIVE, SET_SPLASHSCREEN_ACTIVE, SET_VIEW_SETTINGS } = actions

  return {
    setBasemapActive: () => dispatch({ type: SET_BASEMAP_ACTIVE }),
    setSplashScreenActive: () => dispatch({ type: SET_SPLASHSCREEN_ACTIVE }),
    setViewSettings: settings => dispatch({ type: SET_VIEW_SETTINGS, settings })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Map)
