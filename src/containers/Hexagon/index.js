/*
  URL: https://gist.github.com/Pessimistress/1a4f3f5eb3b882ab4dd29f8ac122a7be
  Title: deck.gl + Mapbox HexagonLayer Example
  Author: Xiaoji Chen (@pessimistress)
  Data URL: https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/3d-hexagon/hexagon-data.csv
  Data Source: https://data.gov.uk
*/
import React, { Component } from 'react'
import { connect } from 'react-redux'
import { HexagonLayer } from '@deck.gl/aggregation-layers'
import { MapboxLayer } from '@deck.gl/mapbox'
import mapboxgl from 'mapbox-gl'

import HexagonUI from 'components/Hexagon'
import Mapbox from 'components/Mapbox'

import authorizationService from 'services/authorizationService'
import dataService from 'services/dataService'

import actions from 'enums/actions'
import scss from './index.module.scss'
import ui from 'config/ui'

class Hexagon extends Component {
  hexagon = null
  map = null
  ui = ui

  componentDidMount() {
    const { accessToken } = mapboxgl

    accessToken && !authorizationService.hasSessionExpired()
      ? this.loadMap()
      : window.location.replace('/')
  }

  componentWillUnmount() {
    const { setSplashScreenActive } = this.props

    setSplashScreenActive()

    this.map.off('idle')
    this.map.off('load')
  }

  loadMap() {
    const {
      hexagon: { settings }
    } = this.props

    const {
      hexagon: {
        navigationControl: { position }
      }
    } = this.ui

    this.map = new mapboxgl.Map(settings)
      .addControl(new mapboxgl.NavigationControl(), position)
      .on('load', () => {
        this.setHexagon()
        this.addLayer()
      })
      .on('idle', () => {
        this.setHexagonSettings()
      })
  }

  setHexagon() {
    const {
      hexagon: {
        params: { coverage, elevationScale, radius, upperPercentile }
      }
    } = this.props

    const {
      hexagon: {
        id,
        props: { colorRange, elevationRange, extruded, material, opacity }
      }
    } = this.ui

    this.hexagon = new MapboxLayer({
      id,
      type: HexagonLayer,
      colorRange,
      coverage,
      data: dataService.hexagonData,
      elevationRange,
      elevationScale,
      extruded,
      getPosition: d => [+d.lng, +d.lat],
      material,
      opacity,
      radius,
      upperPercentile
    })
  }

  addLayer() {
    this.map.addLayer(this.hexagon)
    this.setLayerVisibility()
  }

  setLayerVisibility() {
    const { splashScreen, setSplashScreenActive } = this.props

    if (splashScreen.active) {
      setSplashScreenActive()
    }

    this.map.setLayoutProperty(this.hexagon.id, 'visibility', 'visible')
  }

  setHexagonSettings() {
    const {
      hexagon: { settings },
      setHexagonSettings
    } = { ...this.props }

    settings.bearing = this.map.getBearing()
    settings.center = this.map.getCenter().toArray()
    settings.pitch = this.map.getPitch()
    settings.zoom = this.map.getZoom()

    setHexagonSettings(settings)
  }

  changeInputValue = (id, value) => {
    const {
      hexagon: { params },
      setHexagonParams
    } = { ...this.props }

    params[id] = +value

    setHexagonParams(params)
    setTimeout(() => this.setHexagonParams(id))
  }

  setHexagonParams(param) {
    const {
      hexagon: { params }
    } = this.props

    this.hexagon.setProps({ [param]: params[param] })
  }

  resetHexagonParamsState = () => {
    const {
      hexagon: { params },
      setHexagonParams
    } = { ...this.props }

    const {
      hexagon: { props }
    } = this.ui

    Reflect.ownKeys(params).forEach(param => {
      params[param] = props[param]

      setHexagonParams(params)
      setTimeout(() => this.setHexagonParams(param))
    })
  }

  resetHexagonSettingsState = () => {
    const {
      hexagon: { settings },
      setHexagonSettings
    } = { ...this.props }

    const {
      hexagon: {
        settings: {
          initial: { bearing, center, pitch, zoom }
        }
      }
    } = this.ui

    settings.bearing = bearing
    settings.center = center
    settings.pitch = pitch
    settings.zoom = zoom

    setHexagonSettings(settings)
    setTimeout(() => this.resetHexagonSettings())
  }

  resetHexagonSettings() {
    const {
      hexagon: {
        settings: { bearing, center, pitch, zoom }
      }
    } = this.props

    this.map.setBearing(bearing)
    this.map.setCenter(center)
    this.map.setPitch(pitch)
    this.map.setZoom(zoom)
  }

  logoutHandler = () => authorizationService.logout()
  onChangeInputValueHandler = ({ target: { id, value } }) => this.changeInputValue(id, value)
  resetHexagonParamsStateHandler = () => this.resetHexagonParamsState()
  resetHexagonSettingsStateHandler = () => this.resetHexagonSettingsState()
  returnToTrailsHandler = () => this.props.history.push('/map')

  render() {
    const {
      hexagon: {
        params: { coverage, elevationScale, radius, upperPercentile }
      }
    } = this.props

    return (
      <div className={scss.HexagonUI}>
        <Mapbox />
        <HexagonUI
          coverage={coverage}
          elevationScale={elevationScale}
          radius={radius}
          upperPercentile={upperPercentile}
          logout={this.logoutHandler}
          onChangeInputValue={this.onChangeInputValueHandler}
          resetHexagonParams={this.resetHexagonParamsStateHandler}
          resetHexagonSettings={this.resetHexagonSettingsStateHandler}
          returnToTrails={this.returnToTrailsHandler}
        />
      </div>
    )
  }
}

const mapStateToProps = state => {
  const { hexagon, splashScreen } = state
  return { hexagon, splashScreen }
}

const mapDispatchToProps = dispatch => {
  const { SET_HEXAGON_PARAMS, SET_HEXAGON_SETTINGS, SET_SPLASHSCREEN_ACTIVE } = actions

  return {
    setHexagonParams: params => dispatch({ type: SET_HEXAGON_PARAMS, params }),
    setHexagonSettings: settings => dispatch({ type: SET_HEXAGON_SETTINGS, settings }),
    setSplashScreenActive: () => dispatch({ type: SET_SPLASHSCREEN_ACTIVE })
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Hexagon)
