import actions from 'enums/actions'

const reducer = (state, action) => {
  const {
    SET_BASEMAP_ACTIVE,
    SET_CHART_ELEMENTS,
    SET_COLUMN_CHART,
    SET_DATA_TABLE,
    SET_GROUPED_COLUMN_CHART,
    SET_HEXAGON_PARAMS,
    SET_HEXAGON_SETTINGS,
    SET_MAP_SETTINGS,
    SET_MULTI_SERIES_LINE_CHART,
    SET_SPLASHSCREEN_ACTIVE,
    SET_TRAIL_NAME,
    SET_VIEW_SETTINGS
  } = actions

  switch (action.type) {
    case SET_BASEMAP_ACTIVE:
      const {
        view: { basemap }
      } = { ...state }

      const active = basemap.next
      const next = basemap.active

      basemap.active = active
      basemap.next = next

      return { ...state, view: { ...state.view, basemap: { ...state.view.basemap, basemap } } }

    case SET_CHART_ELEMENTS:
      return {
        ...state,
        chartElements: [...action.settings]
      }

    case SET_COLUMN_CHART:
      return {
        ...state,
        columnChart: { ...state.columnChart, ...action.settings }
      }

    case SET_DATA_TABLE:
      return {
        ...state,
        dataTable: { ...state.dataTable, ...action.settings }
      }

    case SET_GROUPED_COLUMN_CHART:
      return {
        ...state,
        groupedColumnChart: { ...state.groupedColumnChart, ...action.settings }
      }

    case SET_HEXAGON_PARAMS:
      return {
        ...state,
        hexagon: { ...state.hexagon, params: { ...state.hexagon.params, ...action.params } }
      }

    case SET_HEXAGON_SETTINGS:
      return {
        ...state,
        hexagon: { ...state.hexagon, settings: { ...state.hexagon.settings, ...action.settings } }
      }

    case SET_MAP_SETTINGS:
      return {
        ...state,
        map: { ...state.map, settings: { ...state.map.settings, ...action.settings } }
      }

    case SET_MULTI_SERIES_LINE_CHART:
      return {
        ...state,
        multiSeriesLineChart: { ...state.multiSeriesLineChart, ...action.settings }
      }

    case SET_SPLASHSCREEN_ACTIVE:
      const { splashScreen } = { ...state }

      splashScreen.active = !splashScreen.active
      splashScreen.active ? (splashScreen.class = 'active') : (splashScreen.class = 'inactive')

      return {
        ...state,
        splashScreen: { ...state.splashScreen, ...splashScreen }
      }

    case SET_TRAIL_NAME:
      return {
        ...state,
        trail: { ...state.trail, name: action.name }
      }

    case SET_VIEW_SETTINGS:
      return {
        ...state,
        view: { ...state.view, settings: { ...state.view.settings, ...action.settings } }
      }

    default:
      return state
  }
}

export default reducer
