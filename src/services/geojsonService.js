export default {
  setGeoJSON(features, metrics) {
    return {
      type: 'FeatureCollection',
      features: features.map(obj => {
        const feature = {
          type: 'Feature',
          geometry: {
            type: 'Point',
            coordinates: [obj.lon, obj.lat]
          },
          properties: {}
        }

        for (var key in obj) {
          if (metrics.includes(key)) {
            feature.properties[key] = +obj[key].replace(',', '')
            continue
          }

          feature.properties[key] = obj[key]
        }

        return feature
      })
    }
  }
}
