import esriService from 'services/esriService'

export default {
  graphicsLayers: [],
  graphicsLayersHash: {},

  createGraphic(attributes, geometry, symbol, popupTemplate) {
    return new esriService.Graphic({
      attributes,
      geometry,
      popupTemplate,
      symbol
    })
  },

  createPointGraphicsLayer({ features }, id) {
    const gl = new esriService.GraphicsLayer()

    gl.geometryType = 'point'
    gl.visible = false

    features.forEach(({ geometry, properties }) => {
      const geom = {
        type: 'point',
        longitude: properties.lng || geometry.coordinates[0],
        latitude: properties.lat || geometry.coordinates[1]
      }

      const symbol = {
        type: 'picture-marker',
        url: `assets/${id}.png`,
        height: '25px',
        width: '22px'
      }

      gl.add(this.createGraphic(properties, geom, symbol))
    })

    return gl
  },

  createPolygonGraphicsLayer({ features }, id) {
    const gl = new esriService.GraphicsLayer()

    gl.geometryType = 'polygon'
    gl.visible = true

    features.forEach(({ geometry, properties }) => {
      const attributes = {}

      const geom = {
        type: 'polygon',
        rings: geometry.coordinates
      }

      const symbol = {
        type: 'simple-fill',
        color: 'rgba(0, 153, 0, 0.3)',
        style: 'solid',
        outline: {
          color: 'rgb(0, 0, 0)',
          width: '1px'
        }
      }

      const popupTemplate = new esriService.PopupTemplate({
        title: properties.name,
        content: properties.description
      })

      gl.add(this.createGraphic(attributes, geom, symbol, popupTemplate))
    })

    return gl
  },

  createPolylineGraphicsLayer({ features }) {
    const gl = new esriService.GraphicsLayer()

    gl.geometryType = 'polyline'
    gl.visible = false

    features.forEach(({ geometry }) => {
      const attributes = {}

      const geom = {
        type: 'polyline',
        paths: geometry.coordinates
      }

      const symbol = {
        type: 'simple-line',
        color: 'rgb(153, 0, 0)',
        style: 'solid',
        width: '2px'
      }

      gl.add(this.createGraphic(attributes, geom, symbol))
    })

    return gl
  },

  createGraphicsLayersArray(gl, id) {
    this.graphicsLayers.push(gl)
    this.graphicsLayersHash[id] = this.graphicsLayers.length - 1
  },

  /* fc => GeoJSON FeatureCollection object containing feature geometry & properties/attributes */
  createGraphicsLayer(fc, id) {
    const {
      geometry: { type }
    } = fc.features[0]

    switch (type) {
      case 'LineString':
        this.createGraphicsLayersArray(this.createPolylineGraphicsLayer(fc), id)
        return this.createGraphicsLayersArray(this.createPointGraphicsLayer(fc, id), `${id}-point`)

      case 'Point':
        return this.createGraphicsLayersArray(this.createPointGraphicsLayer(fc, id), id)

      case 'Polygon':
        return this.createGraphicsLayersArray(this.createPolygonGraphicsLayer(fc, id), id)

      default:
        throw new Error('default case not expected')
    }
  },

  setGraphicsLayerVisibility(id) {
    this.graphicsLayers[this.graphicsLayersHash[id]].visible = !this.graphicsLayers[
      this.graphicsLayersHash[id]
    ].visible
  }
}
