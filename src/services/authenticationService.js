import appService from 'services/appService'
import authorizationService from 'services/authorizationService'
import httpService from 'services/httpService'
import urls from 'enums/urls'

export default {
  login(props, user) {
    const {
      AUTH: { LOGIN_ENDPOINT }
    } = urls

    httpService
      .post(LOGIN_ENDPOINT, user)
      .then(({ data }) => {
        if (data) {
          authorizationService.setSession(data)
          return appService.initializeApp(props)
        }

        return console.log(`No JSON Web Token Generated:\n`, data)
      })
      .catch(err => {
        console.error('Login Failed:\n', err)
      })
  },

  register(props, user) {
    const {
      AUTH: { REGISTER_ENDPOINT }
    } = urls

    httpService
      .post(REGISTER_ENDPOINT, user)
      .then(({ data }) => {
        data
          ? this.login(props, { username: data.username, password: data.password })
          : console.log(`No User Created:\n`, data)
      })
      .catch(err => {
        console.error('Registration Failed:\n', err)
      })
  }
}
