import { loadModules } from 'esri-loader'

export default {
  BasemapToggle: null,
  Graphic: null,
  GraphicsLayer: null,
  Map: null,
  MapView: null,
  PopupTemplate: null,
  watchUtils: null,

  async loadModules() {
    await loadModules([
      'esri/widgets/BasemapToggle',
      'esri/Graphic',
      'esri/layers/GraphicsLayer',
      'esri/Map',
      'esri/views/MapView',
      'esri/PopupTemplate',
      'esri/core/watchUtils'
    ]).then(([BasemapToggle, Graphic, GraphicsLayer, Map, MapView, PopupTemplate, watchUtils]) => {
      this.BasemapToggle = BasemapToggle
      this.Graphic = Graphic
      this.GraphicsLayer = GraphicsLayer
      this.Map = Map
      this.MapView = MapView
      this.PopupTemplate = PopupTemplate
      this.watchUtils = watchUtils
    })
  }
}
