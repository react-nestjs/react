import graphicsLayerService from 'services/graphicsLayerService'

export default {
  map: null,
  view: null,

  addGraphicsLayers() {
    graphicsLayerService.graphicsLayers
      .filter(gl => gl.visible)
      .forEach(gl => this.addGraphicsLayer(gl))
  },

  addGraphicsLayer(gl) {
    const { geometryType } = gl

    switch (geometryType) {
      case 'polygon':
        return this.map.add(gl, 0)

      case 'polyline':
        return this.map.add(gl, 1)

      case 'point':
        return this.map.add(gl, 2)

      default:
        throw new Error('default case not expected')
    }
  },

  removeGraphicsLayer(gl) {
    this.map.remove(gl)
  },

  goTo(trail) {
    this.view.goTo({
      center: trail.center,
      zoom: trail.zoom
    })
  }
}
