import authorizationService from 'services/authorizationService'
import dataService from 'services/dataService'
import esriService from 'services/esriService'

export default {
  async initializeApp(props) {
    await this.loadEsriModules()
    this.loadData()
    this.loadMap(props)
  },

  async loadEsriModules() {
    await esriService.loadModules()
  },

  loadData() {
    dataService.loadData()
  },

  async loadMap(props) {
    await authorizationService.getMapboxAccessToken()
    props.history.push('/map')
  }
}
