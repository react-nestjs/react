import mapboxgl from 'mapbox-gl'
import moment from 'moment/moment'

import httpService from 'services/httpService'
import urls from 'enums/urls'

export default {
  async getMapboxAccessToken() {
    const {
      API: { MAPBOX_ACCESS_TOKEN_ENDPOINT }
    } = urls

    await httpService
      .get(MAPBOX_ACCESS_TOKEN_ENDPOINT)
      .then(({ data }) => {
        data
          ? this.setMapboxAccessToken(data)
          : console.log(`No Mapbox Access Token Found:\n`, data)
      })
      .catch(err => {
        console.error('getMapboxAccessToken Failed:\n', err)
      })
  },

  setMapboxAccessToken(accessToken) {
    mapboxgl.accessToken = accessToken
  },

  getSessionExpiration() {
    const expiresAt = JSON.parse(sessionStorage.getItem('expiresAt'))
    return moment(expiresAt)
  },

  hasSessionExpired() {
    return moment().isAfter(this.getSessionExpiration())
  },

  setSession(token) {
    const expiresAt = moment().add(token.expiresIn, 'seconds')

    sessionStorage.setItem('accessToken', token.accessToken)
    sessionStorage.setItem('expiresAt', JSON.stringify(expiresAt.valueOf()))
  },

  logout() {
    sessionStorage.removeItem('accessToken')
    sessionStorage.removeItem('expiresAt')

    window.location.replace('/')
    window.location.reload()
  }
}
