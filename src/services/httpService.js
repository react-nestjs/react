import axios from './axiosService'

export default {
  get(url, params = {}) {
    const accessToken = sessionStorage.getItem('accessToken')

    if (accessToken && !axios.defaults.headers.get['Authorization']) {
      axios.defaults.headers.get['Authorization'] = `Bearer ${accessToken}`
    }

    return axios.get(url, params)
  },

  post(url, data) {
    return axios.post(url, data)
  }
}
