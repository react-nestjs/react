import * as fetch from 'd3-fetch'

import graphicsLayerService from 'services/graphicsLayerService'
import httpService from 'services/httpService'

import layers from 'config/layers'
import urls from 'enums/urls'

export default {
  hexagonData: null,
  layers: layers,

  loadData() {
    this.getHexagonData()
    this.getMapData()
  },

  getHexagonData() {
    const {
      HEXAGON: { HEXAGON_DATA_URL }
    } = urls

    fetch
      .csv(HEXAGON_DATA_URL)
      .then(data => {
        data?.length ? (this.hexagonData = data) : console.error('Data Error:\n', data)
      })
      .catch(err => {
        console.error('getHexagonData Failed:\n', err)
      })
  },

  getMapData() {
    const {
      API: { GEOJSON_ENDPOINT }
    } = urls

    this.layers.forEach(layer => {
      const params = {
        fields: layer.fields,
        table: layer.id
      }

      httpService
        .get(GEOJSON_ENDPOINT, { params })
        .then(({ data }) => {
          data?.features.length
            ? graphicsLayerService.createGraphicsLayer(data, layer.id)
            : console.log(`No ${layer.id.toUpperCase()} GraphicsLayer Found:\n`, data)
        })
        .catch(err => {
          console.error('http Failed:\n', err)
        })
    })
  }
}
