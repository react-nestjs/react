export default [
  {
    id: 'biosphere',
    fields: 'name, description, geom'
  },
  {
    id: 'office',
    fields: 'name, description, geom'
  },
  {
    id: 'places',
    fields: 'name, description, geom'
  },
  {
    id: 'trails',
    fields: 'name, description, lat, lng, geom'
  }
]
