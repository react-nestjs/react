import ui from './ui'

import ColumnChartConfig from 'components/Charts/Column/index.config'
import DataTableConfig from 'components/Charts/DataTable/index.config'
import GroupedColumnChartConfig from 'components/Charts/GroupedColumn/index.config'
import MultiSeriesLineChartConfig from 'components/Charts/MultiSeriesLine/index.config'

const { chartElements, hexagon, splashScreen, trail, view } = ui

export default {
  columnChart: ColumnChartConfig,
  dataTable: DataTableConfig,
  groupedColumnChart: GroupedColumnChartConfig,
  multiSeriesLineChart: MultiSeriesLineChartConfig,
  chartElements,
  hexagon: {
    params: {
      coverage: hexagon.params.coverage,
      elevationScale: hexagon.params.elevationScale,
      radius: hexagon.params.radius,
      upperPercentile: hexagon.params.upperPercentile
    },
    settings: {
      bearing: hexagon.settings.bearing,
      center: hexagon.settings.center,
      container: hexagon.settings.container,
      maxZoom: hexagon.settings.maxZoom,
      minZoom: hexagon.settings.minZoom,
      pitch: hexagon.settings.pitch,
      style: hexagon.settings.style,
      zoom: hexagon.settings.zoom
    }
  },
  splashScreen: {
    active: splashScreen.active,
    class: splashScreen.class
  },
  trail: {
    name: trail.name
  },
  view: {
    basemap: {
      active: view.basemap.active,
      next: view.basemap.next
    },
    settings: {
      center: view.settings.center,
      zoom: view.settings.zoom
    }
  }
}
