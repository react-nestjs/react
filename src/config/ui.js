export default {
  chartContainer: {
    bounds: 'parent',
    maxHeight: 800,
    maxWidth: 1200,
    minHeight: 400,
    minWidth: 600,
    style: {
      display: 'flex',
      alignItems: 'center',
      background: 'rgba(255, 255, 255, 0.7)',
      border: '2px solid rgb(140, 130, 120)',
      borderRadius: '10px',
      justifyContent: 'center',
      zIndex: 2
    }
  },
  chartElements: [
    {
      id: 'columnChart',
      active: false,
      class: '',
      name: 'Column Chart'
    },
    {
      id: 'groupedColumnChart',
      active: false,
      class: '',
      name: 'Grouped Column Chart'
    },
    {
      id: 'multiSeriesLineChart',
      active: false,
      class: '',
      name: 'Multi-Series Line Chart'
    },
    {
      id: 'dataTable',
      active: false,
      class: '',
      name: 'Data Tables'
    },
    {
      id: 'trails',
      active: false,
      class: '',
      name: 'Return to Trails'
    },
    {
      id: 'logout',
      active: false,
      class: '',
      name: 'Logout'
    }
  ],
  hexagon: {
    id: 'hexagon',
    navigationControl: {
      position: 'top-left'
    },
    params: {
      coverage: 1,
      elevationScale: 100,
      radius: 1000,
      upperPercentile: 100
    },
    props: {
      coverage: 1,
      colorRange: [
        [1, 152, 189],
        [73, 227, 206],
        [216, 254, 181],
        [254, 237, 177],
        [254, 173, 84],
        [209, 55, 78]
      ],
      elevationRange: [0, 2500],
      elevationScale: 100,
      extruded: true,
      material: {
        ambient: 0.6,
        diffuse: 0.6,
        shininess: 50,
        specularColor: [200, 200, 200]
      },
      opacity: 1,
      radius: 1000,
      upperPercentile: 100
    },
    settings: {
      bearing: -30,
      center: [-1.8, 52.1],
      container: 'mapbox',
      initial: {
        bearing: -30,
        center: [-1.8, 52.0],
        pitch: 50,
        zoom: 6.5
      },
      maxZoom: 18,
      minZoom: 2,
      pitch: 50,
      style: 'mapbox://styles/mapbox/dark-v10',
      zoom: 6.5
    }
  },
  layerElements: [
    {
      id: 'biosphere',
      active: true,
      class: 'active',
      name: 'Biosphere'
    },
    {
      id: 'office',
      active: false,
      class: '',
      name: 'Office'
    },
    {
      id: 'places',
      active: false,
      class: '',
      name: 'Places'
    },
    {
      id: 'trails',
      active: false,
      class: '',
      name: 'Trails'
    },
    {
      id: 'hexagon',
      active: false,
      class: '',
      name: 'Deck.GL'
    },
    {
      id: 'charts',
      active: false,
      class: '',
      name: 'Charts'
    },
    {
      id: 'logout',
      active: false,
      class: '',
      name: 'Logout'
    }
  ],
  layerIcons: [
    {
      id: 'biosphere',
      name: 'Biosphere',
      src: 'assets/biosphere.png',
      height: 16,
      width: 16
    },
    {
      id: 'office',
      name: 'Office',
      src: 'assets/office.png',
      height: 20,
      width: 18
    },
    {
      id: 'places',
      name: 'Places',
      src: 'assets/places.png',
      height: 20,
      width: 18
    },
    {
      id: 'trails',
      name: 'Trails',
      src: 'assets/trails.png',
      height: 20,
      width: 18
    },
    {
      id: 'hexagon',
      name: 'Deck.GL',
      src: 'assets/uber.png',
      height: 18,
      width: 18
    },
    {
      id: 'charts',
      name: 'Charts',
      src: 'assets/charts.png',
      height: 18,
      width: 18
    },
    {
      id: 'logout',
      name: 'Logout',
      src: 'assets/logout.png',
      height: 18,
      width: 18
    }
  ],
  map: {
    navigationControl: {
      position: 'top-left'
    },
    settings: {
      bearing: 0,
      center: [-79.4, 43.4],
      container: 'mapbox',
      maxZoom: 14,
      minZoom: 3,
      pitch: 0,
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 9
    }
  },
  splashScreen: {
    active: false,
    class: 'inactive'
  },
  tableContainer: {
    bounds: 'parent',
    maxHeight: 980,
    maxWidth: 1200,
    minHeight: 560,
    minWidth: 900,
    style: {
      display: 'flex',
      alignItems: 'flex-end',
      background: 'rgba(255, 255, 255, 0.7)',
      border: '2px solid rgb(140, 130, 120)',
      borderRadius: '10px',
      justifyContent: 'center',
      zIndex: 2
    }
  },
  trail: {
    name: 'Select Trail'
  },
  trails: [
    {
      name: 'Select Trail',
      active: true
    },
    {
      name: 'Blue Mountain',
      active: false,
      center: [-76.04, 44.508],
      zoom: 13
    },
    {
      name: 'Charleston Lake',
      active: false,
      center: [-76.04, 44.508],
      zoom: 13
    },
    {
      name: 'Lemoine Point',
      active: false,
      center: [-76.61, 44.223],
      zoom: 14
    },
    {
      name: 'Lyn Valley',
      active: false,
      center: [-75.75, 44.575],
      zoom: 13
    },
    {
      name: 'Mac Johnson',
      active: false,
      center: [-75.75, 44.575],
      zoom: 13
    },
    {
      name: "Seeley's Bay",
      active: false,
      center: [-76.22, 44.485],
      zoom: 14
    }
  ],
  user: {
    password: 'Click_Login',
    username: 'johncampbell@geospatialweb.ca'
  },
  view: {
    basemap: {
      active: 'topo',
      next: 'satellite'
    },
    constraints: {
      minZoom: 2,
      maxZoom: 18
    },
    container: 'map',
    popup: {
      dockOptions: {
        buttonEnabled: false
      }
    },
    settings: {
      center: [-76.3, 44.5],
      zoom: 10
    }
  }
}
